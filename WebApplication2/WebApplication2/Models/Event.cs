﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Event

    {
        public string RoomName { get; set; }
        //public string RoomID { get; set; }
        //public string EventID { get; set; }
        public string  StartTime { get; set; }
        public string EndTime { get; set; }

        public string Subject { get; set; }
        public string Requestor { get; set; }

    }
}