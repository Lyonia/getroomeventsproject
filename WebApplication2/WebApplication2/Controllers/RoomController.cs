﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication2.Models;
using System.IO;
using System.Linq;

namespace WebApplication2.Controllers
{
    public class RoomController : Controller
    {

        // GET: Room
        public ActionResult Index()
        {
            List<Room> rooms = new List<Room>();

            var csvColumn = new List<string>();

            using (var rd = new StreamReader(@"C:\Temp\RoomList.csv"))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(';');
                    csvColumn.Add(splits[0]);
                }
            }
            int i = 1;
            foreach (var element in csvColumn)
            {

                string str = element;
                string[] tokens = str.Split('(');
                string CrName = tokens[0];// + " " + tokens[1];
                string[] tokens2 = CrName.Split(',');
                CrName = tokens2[1];

                string CrCapacity = tokens[1];
                CrCapacity = CrCapacity.Remove(CrCapacity.Length - 1);
                rooms.Add(new Room() { Name = CrName, Capacity = CrCapacity });// capacity="12"});
                i++;
            }

            return View("Index", rooms);
        }

       
        
      
        public ActionResult ShowEvents(string roomName) 
        {
                List<Event> events = new List<Event>();

            //----------EventsDetails----------------
                roomName = roomName.Trim();
            
                string path = @"C:\Temp\Rooms.csv";
              
                string EndDate;
                string Requestor;
                string Subject;
                string StartDate;

                List<string> SearchedLines = new List<string>();
                int counter = 0;
                string line;
                // Read the file and display it line by line.
                System.IO.StreamReader file = new System.IO.StreamReader(path);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Contains(roomName))
                    {
                        string evcount = System.IO.File.ReadLines(path).Skip(counter + 1).Take(2).First();

                        if (evcount == "0")
                        {
                            events.Add(new Event()
                            {
                                RoomName = roomName,
                                StartTime = "0",
                                EndTime = "0",
                                Requestor = "0",
                                Subject = "0"
                            });
                        }
                        else
                        {
                            int c = Int32.Parse(evcount);
                            for (int i = 0; i < c; i++)
                            {
                               // Console.WriteLine("there are {0} events", counter.ToString());
                               // counter = counter + i + c;
                                StartDate = System.IO.File.ReadLines(path).Skip(counter+2).Take(2).First();
                                //  Console.WriteLine(StartDate);
                                EndDate = System.IO.File.ReadLines(path).Skip(counter + 3).Take(2).First();
                                // Console.WriteLine(EndDate);
                                Requestor = System.IO.File.ReadLines(path).Skip(counter + 4).Take(2).First();
                                // Console.WriteLine(Requestor);
                                Subject = System.IO.File.ReadLines(path).Skip(counter + 5).Take(2).First();
                                // Console.WriteLine(Subject);
                                events.Add(new Event()
                                {
                                    RoomName = roomName,
                                    StartTime = StartDate,
                                    EndTime = EndDate,
                                    Requestor = Requestor,
                                    Subject = Subject
                                });
                            counter = counter + 4;
                        }
                     }
                    }
                    counter++;
                }
                //Console.WriteLine(events.Count.ToString());
                //Console.WriteLine("Done");
                //Console.ReadKey();
                return View("ShowEvents", events);
        }
    }
}






