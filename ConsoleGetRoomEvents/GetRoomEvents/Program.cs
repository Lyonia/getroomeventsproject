﻿using Autofac;
using GetRoomEvents.Interfaces;
using System;


namespace GetRoomList
{

    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            Container = GetRoomEvents.Core.AutofacConfig.Register();

            using (var scope = Container.BeginLifetimeScope())
            {
                var roomService = scope.Resolve<IRoomService>();  // Room service instance
                var roomList = roomService.GetRoomList();
                var roomEvents = roomService.GetRoomEvents();  
                

                if (!roomEvents.Succeed || !roomList.Succeed)
                {
                    Console.WriteLine("Application throws an error");
                }

            }

            Console.WriteLine("Press any key ");
            Console.ReadKey();
        }
        
    }
}


