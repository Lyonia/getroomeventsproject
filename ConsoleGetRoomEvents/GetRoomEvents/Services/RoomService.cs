﻿using GetRoomEvents.Interfaces;
using GetRoomEvents.Models;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GetRoomEvents.Services
{
    class RoomService : IRoomService
    {
        IConfig config;

        public RoomService(IConfig conf)
        {
            this.config = conf;
        }

        //Get and write to CSV file conference rooms list from specific Distribution list
        public ResultModel GetRoomList()
        {
            Console.WriteLine("Room list:");
            var result = new ResultModel();
            try
            {
                ExchangeService service = new ExchangeService();
                service.Credentials = new WebCredentials(config.Email, config.EmailPass); 
                service.Url = new Uri(config.Url);

                EmailAddress myRoomList = config.DlEmail;  
                System.Collections.ObjectModel.Collection<EmailAddress> myRoomAddresses = service.GetRooms(myRoomList);

                string path = config.CsvPath;  
                using (var file = File.CreateText(path))
                {
                    foreach (EmailAddress address in myRoomAddresses)
                    {
                        file.WriteLine(string.Join(",", address.Name));
                        Console.WriteLine("Email Address: {0}", address.Address);
                    }
                }
            }
            catch(Exception e)
            {
                result.Succeed = false;
                result.Error = e.Message;
            }

            return result;
        }

        //Get and write to CSV file conference rooms events for 1 upcoming day
        public ResultModel GetRoomEvents()
        {
            var result = new ResultModel();
            try
            {
                ExchangeService service = new ExchangeService();
                service.Credentials = new WebCredentials(config.Email, config.EmailPass);
                service.Url = new Uri(config.Url);

                //get rooms e-mail addresses
                EmailAddress myRoomList = config.DlEmail;
                System.Collections.ObjectModel.Collection<EmailAddress> myRoomAddresses = service.GetRooms(myRoomList);
                string path = @"C:\Temp\Events.csv";
                //recording conference rooms events to csv
                using (StreamWriter writer = new StreamWriter(path, false))
                {

                    foreach (EmailAddress address in myRoomAddresses)
                    {
                        List<AttendeeInfo> attendees = new List<AttendeeInfo>();
                        attendees.Add(address.Address);
                        AvailabilityOptions myOptions = new AvailabilityOptions();

                        //determening how much days till weekend left
                        DateTime start = DateTime.Now;
                        DateTime stop = DateTime.Now.AddDays(1);
                        int days = 0;
                        while (start.DayOfWeek != stop.DayOfWeek)
                        {
                            if (DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
                            {
                                ++days;
                            }
                            start = start.AddDays(1);
                        }
                        GetUserAvailabilityResults myAvailablityResults = service.GetUserAvailability(attendees,
                            new TimeWindow(DateTime.Now, DateTime.Now.AddDays(days)), AvailabilityData.FreeBusy, myOptions);

                        string str1 = address.Name;
                        string[] tokens1 = str1.Split('(');
                        string CrName = tokens1[0];
                        string[] tokens2 = CrName.Split(',');
                        CrName = tokens2[1];
                        CrName = CrName.Remove(0, 1);
                        writer.WriteLine(string.Join(",", CrName));
                        Console.WriteLine(CrName);

                        foreach (AttendeeAvailability myAvailablity in myAvailablityResults.AttendeesAvailability)
                        {
                            if (myAvailablity.CalendarEvents.Count > 0)
                            {
                                writer.WriteLine(string.Join(",", myAvailablity.CalendarEvents.Count.ToString()));
                            }
                            else
                            {
                                writer.WriteLine(string.Join(",", "0"));
                            }
                            foreach (CalendarEvent calendarItem in myAvailablity.CalendarEvents)
                            {
                                writer.WriteLine(string.Join(",", calendarItem.StartTime.ToString()));// "Start Time :" + 
                                writer.WriteLine(string.Join(",", calendarItem.EndTime.ToString()));//"End Time  :" + 
                                                                                                    //Console.WriteLine("FreeBusyStatus  : " + calendarItem.FreeBusyStatus.ToString());
                                                                                                    //Console.WriteLine("Location  : " + calendarItem.Details.Location);
                                if (calendarItem.Details.Subject != null)
                                {
                                    string str = calendarItem.Details.Subject;
                                    string[] tokens = str.Split(' ');
                                    string requestor = tokens[0] + " " + tokens[1];
                                    writer.WriteLine(string.Join(",", (requestor)), Encoding.GetEncoding("ISO-8859-1"));//"Requestor  :" + 

                                    string subject = calendarItem.Details.Subject.Remove(0, requestor.Length);
                                    writer.WriteLine(string.Join(",", subject), Encoding.GetEncoding("ISO-8859-1"));// "Subject  : " + 
                                }
                                else
                                {
                                    writer.WriteLine(string.Join(",", "unknown"));
                                    writer.WriteLine(string.Join(",", "unknown"));
                                }
                            }
                        }
                        writer.WriteLine(string.Join(",", "--------------------------------"));
                        writer.WriteLine(string.Join(",", " "));
                    }
                }
                Console.WriteLine("Done");
                Console.ReadKey();
            }
            catch(Exception e)
            {
                result.Succeed = false;
                result.Error = e.Message;
            }

            return result;
        }
    }
}
