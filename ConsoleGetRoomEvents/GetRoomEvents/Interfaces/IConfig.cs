﻿namespace GetRoomEvents.Interfaces
{
    interface IConfig
    {
        string Email { get; }
        string EmailPass { get;}
        string Url { get; }
        string DlEmail { get; }
        string CsvPath { get; }
    }
}
