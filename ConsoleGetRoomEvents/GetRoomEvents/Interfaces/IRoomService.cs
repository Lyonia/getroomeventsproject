﻿using GetRoomEvents.Models;

namespace GetRoomEvents.Interfaces
{
    interface IRoomService
    {
        ResultModel GetRoomEvents();
        ResultModel GetRoomList();
    }
}
