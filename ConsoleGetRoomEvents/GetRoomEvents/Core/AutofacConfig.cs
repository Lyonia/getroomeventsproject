﻿using Autofac;
using GetRoomEvents.Interfaces;
using GetRoomEvents.Services;


namespace GetRoomEvents.Core
{
    public static class AutofacConfig
    {
        public static IContainer Register()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Config>().As<IConfig>();
            builder.RegisterType<RoomService>().As<IRoomService>();

            return builder.Build();

        }
    }
}
