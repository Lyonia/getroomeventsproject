﻿using GetRoomEvents.Interfaces;

namespace GetRoomEvents.Core
{
    class Config : IConfig
    {
        public string Email => Properties.Settings.Default.Email;

        public string EmailPass => Properties.Settings.Default.EmailPass;

        public string Url => Properties.Settings.Default.Uri;

        public string DlEmail => Properties.Settings.Default.DlEmail;

        public string CsvPath => Properties.Settings.Default.CsvPath;
    }
}
